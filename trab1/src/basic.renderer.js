// ALUNO: Rodrigo Dottori de Oliveira
// DRE: 116015658

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(global = global || self, factory(global.BasicRenderer = {}));
}(this, (function (exports) { 'use strict';

	function testeTriangulo(x, y, vertices) {
			var n1x = vertices[0][1]-vertices[1][1];
			var n1y = -vertices[0][0]+vertices[1][0];
			var l1x = (x-vertices[0][0])*n1x;
			var l1y = (y-vertices[0][1])*n1y;
			var l1 = l1x+l1y;

			var n2x = vertices[2][1]-vertices[1][1];
			var n2y = -vertices[2][0]+vertices[1][0];
			var l2x = (x-vertices[1][0])*n2x;
			var l2y = (y-vertices[1][1])*n2y;
			var l2 = l2x+l2y;

			var n3x = vertices[0][1]-vertices[2][1];
			var n3y = -vertices[0][0]+vertices[2][0];
			var l3x = (x-vertices[2][0])*n3x;
			var l3y = (y-vertices[2][1])*n3y;
			var l3 = l3x+l3y;

			if (l1 >= 0 && -l2 >= 0 && -l3 >= 0) return true;
			else return false;
	}

	// Função auxiliar para o teste de winding number
	function estaAEsquerda(x0, y0, x1, y1, x2, y2) {
		return ((x1-x0)*(y2-y0)) - ((x2-x0)*(y1-y0));
	}

	// Função auxiliar para o teste de winding number
	function testeIntersecao(x, y, vertice1, vertice2) {
		if (vertice2[1] <= y) {
			if (vertice1[1] > y) {
				if (estaAEsquerda(vertice2[0], vertice2[1], vertice1[0], vertice1[1], x, y) > 0) return 1;
			}
		}
		else if (vertice1[1] <= y) {
			if (estaAEsquerda(vertice2[0], vertice2[1], vertice1[0], vertice1[1], x, y) < 0) return -1;
		}

		return 0;
	}

    function inside( x, y, primitive ) {
			if (primitive.shape == "circle") {
				// Em caso de transformação trata o círculo como uma elipse
				if (primitive.hasOwnProperty('xform')) {
					if ( ((x-primitive.center[0])*(x-primitive.center[0])/(primitive.radiusX*primitive.radiusX)) +
						((y-primitive.center[1])*(y-primitive.center[1])/(primitive.radiusY*primitive.radiusY)) <= 1)
							return true;
					else return false;
				}

				else {
					if ( ((x-primitive.center[0])*(x-primitive.center[0])) + ((y-primitive.center[1])*(y-primitive.center[1]))
						<= primitive.radius*primitive.radius ) return true;
					else return false;
				}
			}

			else if (primitive.shape == "triangle") {
				return testeTriangulo(x,y,primitive.vertices);
			}

			else if (primitive.shape == "polygon") {
				// Usa winding number (não testa se o polígono é convexo ou não; usa o mesmo método para todos)
				// Baseado no método apresentado em http://geomalgorithms.com/a03-_inclusion.html
				var windingNumber = 0;

				// Teste com triangulação simples (para polígonos convexos)
				/* for (var i = 1; i < numDeVertices-1; i++) {
					if (testeTriangulo(x,y,[primitive.vertices[0], primitive.vertices[i], primitive.vertices[i+1]])) return true;
				}*/

				windingNumber += testeIntersecao(x, y, primitive.vertices[0], primitive.vertices[primitive.vertices.length-1]);
				for (var i = 1; i < primitive.vertices.length; i++) {
					windingNumber += testeIntersecao(x, y, primitive.vertices[i], primitive.vertices[i-1]);
				}

				return windingNumber != 0;
			}

            return false;
    }


    function Screen( width, height, scene ) {
        this.width = width;
        this.height = height;
        this.scene = this.preprocess(scene);
        this.createImage();
    }

    Object.assign( Screen.prototype, {

            preprocess: function(scene) {
                var preprop_scene = [];

                for( var primitive of scene ) {
                    preprop_scene.push( primitive );
                }

                return preprop_scene;
            },

            createImage: function() {
                this.image = nj.ones([this.height, this.width, 3]).multiply(255);
            },

            rasterize: function() {
                var color;

                // In this loop, the image attribute must be updated after the rasterization procedure.
                for( var primitive of this.scene ) {

					// Implementação das transformações
					// A implementação para o círculo só funciona para certas transformações simples
					if (primitive.hasOwnProperty('xform')) {
						if (primitive.shape == "circle") {
							primitive.center[0] = primitive.center[0]*primitive.xform[0][0] + primitive.center[1]*primitive.xform[0][1] + primitive.xform[0][2];
							primitive.center[1] = primitive.center[0]*primitive.xform[1][0] + primitive.center[1]*primitive.xform[1][1] + primitive.xform[1][2];
							primitive.radiusX = primitive.radius*primitive.xform[0][0] + primitive.radius*primitive.xform[0][1] + primitive.xform[0][2];
							primitive.radiusY = primitive.radius*primitive.xform[1][0] + primitive.radius*primitive.xform[1][1] + primitive.xform[1][2];
						}
						else {
							for (var vertice of primitive.vertices) {
								vertice[0] = vertice[0]*primitive.xform[0][0] + vertice[1]*primitive.xform[0][1] + primitive.xform[0][2];
								vertice[1] = vertice[0]*primitive.xform[1][0] + vertice[1]*primitive.xform[1][1] + primitive.xform[1][2];
							}
						}
					}

					// Definição da bounding box
					// Achar menor e maior x e y dos vértices do polígono
					// Depois, o loop vai ser restrito só a essa região
					// A diferença na performance pode ser verificada no exemplo do leão
					var minX = this.width, maxX = 0, minY = this.height, maxY = 0;
					if (primitive.shape == "circle") {
						if (primitive.hasOwnProperty('xform')) {
							minX = primitive.center[0]-primitive.radiusX;
							maxX = primitive.center[0]+primitive.radiusX;
							minY = primitive.center[1]-primitive.radiusY;
							maxY = primitive.center[1]+primitive.radiusY;
						}
						else {
							minX = primitive.center[0]-primitive.radius;
							maxX = primitive.center[0]+primitive.radius;
							minY = primitive.center[1]-primitive.radius;
							maxY = primitive.center[1]+primitive.radius;
						}
					}
					else {
						for ( var vertice of primitive.vertices ) {
							if (vertice[0] < minX) minX = vertice[0];
							if (vertice[0] > maxX) maxX = vertice[0];
							if (vertice[1] < minY) minY = vertice[1];
							if (vertice[1] > maxY) maxY = vertice[1];
						}
					}

                    for (var i = minX; i < maxX; i++) {
                        var x = i + 0.5;
                        for( var j = minY; j < maxY; j++) {
                            var y = j + 0.5;

                            // First, we check if the pixel center is inside the primitive 
                            if ( inside( x, y, primitive ) ) {
                                // only solid colors for now
                                color = nj.array(primitive.color);
								var xFinal = i, yFinal = j;

                                this.set_pixel( xFinal, this.height - (yFinal + 1), color );
                            }
                        }
                    }
                }
            },

            set_pixel: function( i, j, colorarr ) {
                // We assume that every shape has solid color
                this.image.set(j, i, 0,    colorarr.get(0));
                this.image.set(j, i, 1,    colorarr.get(1));
                this.image.set(j, i, 2,    colorarr.get(2));
            },

            update: function () {
                // Loading HTML element
                var $image = document.getElementById('raster_image');
                $image.width = this.width; $image.height = this.height;

                // Saving the image
                nj.images.save( this.image, $image );
            }
        }
    );

    exports.Screen = Screen;
})));

